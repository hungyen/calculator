//
//  CalculatorAppDelegate.h
//  Calculator
//
//  Created by Chen Hung-Yen on 9/29/12.
//  Copyright (c) 2012 ZeroZone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
