//
//  main.m
//  Calculator
//
//  Created by Chen Hung-Yen on 9/29/12.
//  Copyright (c) 2012 ZeroZone. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CalculatorAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CalculatorAppDelegate class]));
    }
}
