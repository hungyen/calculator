//
//  CalculatorBrain.h
//  Calculator
//
//  Created by Chen Hung-Yen on 9/29/12.
//  Copyright (c) 2012 ZeroZone. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculatorBrain : NSObject

- (void)pushOperand:(double)operand;
- (double)performOperation:(NSString *)operation;

@property (readonly) id program;

+ (double)runProgram:(id)program;
+ (NSString *)descriptionOfProgram:(id)program;

@end
