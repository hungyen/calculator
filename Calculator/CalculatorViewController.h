//
//  CalculatorViewController.h
//  Calculator
//
//  Created by Chen Hung-Yen on 9/29/12.
//  Copyright (c) 2012 ZeroZone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *display;

@end
