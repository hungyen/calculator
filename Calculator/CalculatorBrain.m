//
//  CalculatorBrain.m
//  Calculator
//
//  Created by Chen Hung-Yen on 9/29/12.
//  Copyright (c) 2012 ZeroZone. All rights reserved.
//

#import "CalculatorBrain.h"

@interface CalculatorBrain()
@property (nonatomic, strong) NSMutableArray *programStack;

@end

@implementation CalculatorBrain

@synthesize programStack = _programStack;

- (NSMutableArray *)programStack{
    if(_programStack == nil) _programStack = [[NSMutableArray alloc] init];
    return _programStack;
}

- (void)pushOperand:(double)operand{
    [self.programStack addObject:[NSNumber numberWithDouble:operand]];
    NSLog(@"pushOperand, push operand \"%f\" to the stack", operand);
}


- (double)performOperation:(NSString *)operation{
    [self.programStack addObject:operation];
    NSLog(@"performOperation, push \"%@\" to the stack", operation);
    return [CalculatorBrain runProgram:self.program];
}

- (id)program
{
    return [self.programStack copy];
}

+ (NSString *)descriptionOfProgram:(id)program
{
    return @"the is description of program";
}

+ (double)popOperandOffStack:(NSMutableArray *)stack{
    double result = 0;
    
    id topOfStack = [stack lastObject];
    if(topOfStack) [stack removeLastObject];
    
    if([topOfStack isKindOfClass:[NSNumber class]]){
        result = [topOfStack doubleValue];
        NSLog(@"Got number: %f", result);
    }
    else if([topOfStack isKindOfClass:[NSString class]]){
        NSString *operation = topOfStack;
        NSLog(@"Got operation: %@", topOfStack);
        
        if ([operation isEqualToString:@"+"]){
            result = [self popOperandOffStack:stack] + [self popOperandOffStack:stack];
        } else if ([@"*" isEqualToString:operation]){
            result = [self popOperandOffStack:stack] * [self popOperandOffStack:stack];
        } else if ([@"-" isEqualToString:operation]){
            double opNumber = [self popOperandOffStack:stack];
            result = [self popOperandOffStack:stack] - opNumber;
        } else if ([@"/" isEqualToString:operation]){
            double opNumber = [self popOperandOffStack:stack];
            result = [self popOperandOffStack:stack] / opNumber ;
        }
        
    }
    else{
        NSLog(@"Can't not check the stack type");
    }
    
    return result;
}

+ (double)runProgram:(id)program
{
    NSMutableArray *stack;
    if([program isKindOfClass:[NSArray class]]){
        stack = [program mutableCopy];
    }
    return [self popOperandOffStack:stack];
}

@end
